const VALID_PICKS = ["rock", "paper", "scissors"];
const COMPUTER_WAIT_MIN = 1000;
const COMPUTER_WAIT_MAX = 3000;
const TIME_BETWEEN_ROUNDS = 1500;

let round = 1;
let playerScore = 0;
let opponentScore = 0;
let playerSelection = null;
let opponentSelection = null;
const picksContainer = document.querySelector(".picks-container");
const picks = document.querySelectorAll(".pick");
const playerMessage = document.querySelector(".player-message");
const opponentMessage = document.querySelector(".opponent-message");
const loader = document.querySelector(".loader");
const opponentPick = document.querySelector(".opponent-pick");
const roundCounter = document.querySelector(".round-counter");
const roundMessage = document.querySelector(".round-message");
const playerStars = document.querySelectorAll(".player-star");
const opponentStars = document.querySelectorAll(".opponent-star");
const allStars = document.querySelectorAll(".star");
const gameMessage = document.querySelector(".game-message");
const gameScore = document.querySelector(".game-score");
const playAgain = document.querySelector(".play-again-btn");
const playWrapper = document.querySelector(".play-wrapper");
const winContainer = document.querySelector(".win-container");

const movesToImagesTable = {
  rock: "./images/close-hand.svg",
  paper: "./images/open-hand.svg",
  scissors: "./images/peace-sign-hand.svg",
  question: "./images/question-mark.svg",
};

function getRoundResult() {
  if (playerSelection === opponentSelection) {
    return 0;
  }
  if (
    (playerSelection === "rock" && opponentSelection === "scissors") ||
    (playerSelection === "scissors" && opponentSelection === "paper") ||
    (playerSelection === "paper" && opponentSelection === "rock")
  ) {
    return 1;
  }
  return -1;
}

function opponentPlay() {
  return VALID_PICKS[Math.floor(Math.random() * VALID_PICKS.length)];
}

function interpretRound(roundResult) {
  if (roundResult === 1) {
    return "You Win!";
  } else if (roundResult === -1) {
    return "You Lose!";
  }
  return "It's a Tie!";
}

function hideOtherPicks() {
  picks.forEach(function (item) {
    if (item instanceof HTMLElement && item.dataset.pick != playerSelection) {
      item.classList.add("hide");
    }
  });
}

function displayAllPicks() {
  picks.forEach(function (item) {
    item.classList.remove("hide");
  });
}

function playerSelect(event) {
  if (event.target.classList.contains("pick")) {
    playerSelection = event.target.dataset.pick;
    playerMessage.textContent = "You made a pick";
    hideOtherPicks();
    if (opponentSelection != null) {
      updateScore();
    }
  }
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

function opponentSelect() {
  opponentSelection = opponentPlay();
  opponentMessage.textContent = "Opponent made a pick";
  opponentPick.setAttribute("src", movesToImagesTable.question);
  loader.classList.add("hide");
  opponentPick.classList.remove("hide");
  if (playerSelection != null) {
    updateScore();
  }
}

function setFullStar(stars, index) {
  stars[index].innerHTML = "&starf;";
}

function updateScore() {
  ++round;
  const roundResult = getRoundResult();
  if (roundResult === 1) {
    setFullStar(playerStars, playerScore);
    ++playerScore;
  } else if (roundResult === -1) {
    setFullStar(opponentStars, opponentScore);
    ++opponentScore;
  }
  opponentPick.setAttribute("src", movesToImagesTable[opponentSelection]);
  roundMessage.textContent = interpretRound(roundResult);
  if (playerScore < 5 && opponentScore < 5) {
    setTimeout(playRound, TIME_BETWEEN_ROUNDS);
  } else {
    setTimeout(gameOver, TIME_BETWEEN_ROUNDS);
  }
}

function gameOver() {
  if (playerScore > opponentScore) {
    gameMessage.textContent = "You win!";
  } else {
    gameMessage.textContent = "You lose ...";
  }
  gameScore.textContent = `${playerScore} : ${opponentScore}`;
  playAgain.addEventListener("mousedown", playGame, { once: true });
  playWrapper.classList.add("hide");
  winContainer.classList.remove("hide");
}

function roundReset() {
  displayAllPicks();
  playerMessage.textContent = "Take your pick";
  opponentMessage.textContent = "Waiting for opponent";
  loader.classList.remove("hide");
  opponentPick.classList.add("hide");
  opponentPick.setAttribute("src", movesToImagesTable.question);
  playerSelection = null;
  opponentSelection = null;
  roundMessage.textContent = "";
}

function playRound() {
  roundCounter.textContent = `Round ${round}`;
  roundReset();

  const options = { once: true };
  picksContainer.addEventListener("mousedown", playerSelect, options);
  setTimeout(
    opponentSelect,
    getRandomArbitrary(COMPUTER_WAIT_MIN, COMPUTER_WAIT_MAX)
  );
}

function resetStars() {
  allStars.forEach(function (star) {
    star.innerHTML = "&star;";
  });
}

function resetToInitialState() {
  round = 1;
  playerScore = 0;
  opponentScore = 0;
  resetStars();
  roundReset();
  winContainer.classList.add("hide");
  playWrapper.classList.remove("hide");
}

function playGame() {
  resetToInitialState();
  playRound();
}

playGame();
